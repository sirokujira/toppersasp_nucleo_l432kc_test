=====================================================================
                    NUCLEO_L432KC(GCC)ターゲット依存部 
                                  Last Modified:2017 Mar 06 13:07:48
=====================================================================

(1) 対応しているターゲットシステムの種類・構成

NUCLEO_L432KC(GCC)ターゲット依存部は，STM32L432KC(Cortex-M4F)を搭載し
たSTマイクロ社のNUCLEO_L432KCボードをサポートする．プロセッサ依存部と
して，ST32L4XX_STM32CUBE(GCC)プロセッサ依存部をプロセッサ依存部として
ARM_Mプロセッサ依存部を使用する．


(2) 使用する開発環境と動作検証した条件（バージョン，オプション等）
・ライブラリ

デバイスドライはSTM32Cube付属のライブラリを使用している．これらのファ
イルのライセンスはTOPPERSライセンスでないため，注意すること．

バージョンは次の通りである．

 L4 V 1.6.0

・コンパイラ

以下の2種類のコンパイラで動作確認を行った．

GCC 5.4.1  (Launchpad_5.4_2016q3) 
GCC 4.8.3  (Atollic TrueStudio付属)

・デバッグ環境

デバッグ環境としては，JTAGデバッガにST-LINK，Atollic TrueStudioを用い
た環境で動作確認を行っている．

動作確認した TrueStudio は以下のバージョンである．

    V7.1.0
    
TrueStudioは以下のページから無償版のLITEをダウンロードして使用した．

    http://timor.atollic.com/truestudio/

(3) ターゲット定義事項の規定


(4) メモリマップ

標準
  内蔵FlashROM
  ・アドレス : 0x08000000
  ・サイズ   : 256KB
  ・配置セクション
   ・vectorセクション
   ・textセクション
   ・rodataセクション
   
  内蔵RAM
  ・アドレス : 0x20000000
  ・サイズ   : 48KB
    ・dataセクション
    ・bssセクション
    
(5) シリアルインタフェースドライバの情報

コンソール出力には，仮想COMポートに接続されているUSARTのチャネル2を用
いる．通信フォーマットは以下の通りである．

  ・1152200bps, Data 8bit, Parity none, Stop 1bit, Flow control none

(6) システムログ機能の情報

システムログの低レベル出力は，UART2を用いる．通信フォーマットは，
115200bps, 8bit, non-parity, 1topbitである．

(7) Atollic TrueStudio の使用

本パッケージは Atollic TrueStudio をサポートしている．
TrueStudio は，Free版(サイズ制限なし)を以下からダウンロード可能である．

http://timor.atollic.com/truestudio/

プロジェクトのビルドと実行方法

./truestudio/asp/.cproject をTrueStudioで開く．ワークスペースの指定の
画面が表示されるため，./truestudioをワークスペースとして指定する．

プロジェクト・エクスプローラから"asp"を選択する．

メニュー -> プロジェクト -> プロジェクトのビルド  を選択するとビルド
が開始される．

メニュー -> 実行 -> デバッグ構成を選択．

"デバッグ構成"のダイアログが表示される．

左側のツリーから"組込みC/C++アプリケーション"を展開する．

"asp_debug"が表示されるため，選択して右下の"デバッグ"のボタンを押すと
ロードされ，デバッグが開始される．

ロード直後の状態で停止しているため，"実行"→"再開"で実行が開始される．
次にmain_taskでブレークポイントが置いてあるのため停止する．"実行"→"再
開"を選択するとタスク群が実行される．

2回目以降は，メニュー -> 実行 -> デバッグ でデバッグ可能である．
   
(8) 他のNUCLEOボードのサポート

本パッケージはSTM32FCUBEに含まれているファイルを用いて，デバイス等の初
期化を行っているため，他のNUCLEOも容易にサポート可能である．

L432KCを例に手順をまとめる．

./target/nucleo_l432kc_gcc/stm32cube に以下のファイルをコピー

\STM32Cube_FW_F4_V1.9.0\Projects\STM32L432KC-Nucleo\Templates\Inc\stm32l4xx_hal_conf.h
\STM32Cube_FW_F4_V1.9.0\Projects\STM32L432KC-Nucleo\Templates\Src\system_stm32l4xx.c
\STM32Cube_FW_F4_V1.9.0\Projects\STM32L432KC-Nucleo\Templates\Src\main.c 
\STM32Cube_FW_F4_V1.9.0\Drivers\BSP\STM32L4xx-Nucleo\stm32l4xx_nucleo_32.h
\STM32Cube_FW_F4_V1.9.0\Drivers\BSP\STM32L4xx-Nucleo\stm32l4xx_nucleo_32.c

それぞれ次のように変更

stm32l4xx_hal_conf.h
・必要なHALのコメントアウトを外す
system_stm32l4xx.c
・SystemInitの最初にあるCPACRの初期化(FPUの初期化)をコメントアウト
main.c 
・SystemClock_Config()関数のみ残して，ファイル名をsystemclock_config.cに
  変更．
stm32l4xx_nucleo_32.c
・必要でない機能をコメントアウト

./arch/arm_m_gcc/stm32l4xx_stm32cube/ 以下に 
STM32Cube_FW_F4_V1.6.0\Drivers 以下の必要なファイルをコピー．面倒な場
合は全てコピーしてかまわない．

./target/nucleo_l432kc_gcc/ 以下のファイルを変更

Makefile.target
22行目 : 使用するチップの型番を指定
CDEFS := $(CDEFS) -DSTM32L432xx

stm32l432kc.ld
メモリマップを使用するチップに合わせて変更．
MEMORY
{
  FLASH (rx)      : ORIGIN = 0x08000000, LENGTH = 256K
  SRAM (xrw)      : ORIGIN = 0x20000000, LENGTH = 48K
}

target_config.h
割込みの最大数を使用するチップに合わせて変更(必ず+16すること)．
#define TMAX_INTNO (83 + 16)

クロック周波数を使用するチップに合わせて変更．
#define SYS_CLOCK		80000000

target_config.c
target_initialize() を使用するチップに合わせて初期化処理を追加．
usart_early_init() を使用するチップに合わせて初期化処理を追加．

全ファイル
L432KC を，使用するチップの型番にリプレース．


(9) ディレクトリ構成・ファイル構成
  ./nucleo_l432kc_gcc
   ./E_PACKAGE
   ./gdb.ini
   ./Makefile.target
   ./MANIFEST
   ./stm32l432kc.ld
   ./target.tf
   ./target_asm.inc
   ./target_cfg1_out.h
   ./target_check.tf
   ./target_config.c
   ./target_config.h
   ./target_kernel.h
   ./target_rename.def
   ./target_rename.h
   ./target_serial.cfg
   ./target_serial.h
   ./target_sil.h
   ./target_stddef.h
   ./target_syssvc.h
   ./target_test.h
   ./target_timer.cfg
   ./target_timer.h
   ./target_unrename.h
   ./target_user.txt
   ./stm32cube
     ./stm32l4xx_hal_conf.h
     ./stm32l4xx_nucleo_32.c
     ./stm32l4xx_nucleo_32.h
     ./system_stm32l4xx.c
     ./systemclock_config.c
   ./truestudio
     
                        
(10)変更履歴
2017/03/06
新規作成
./target_config.h

以上．
